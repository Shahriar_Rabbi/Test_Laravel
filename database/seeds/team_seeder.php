<?php

use Illuminate\Database\Seeder;

class team_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'name' => str_random(10),
            'member' => str_random(10),
            'age' => str_random(10),
            'average_age' => str_random(10),

        ]);
    }
}
