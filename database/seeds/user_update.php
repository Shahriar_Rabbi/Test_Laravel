<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class user_update extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data_array =[
            'name' => str_random(10),
            'email' => str_random(10),
        ];
        $data_array2 =array(

            array(

                'name'=>'Pondit',

                'email'=>'info@pon42.com',
                'created_at' => '2016-10-30 ',
                'updated_at' => '2016-10-30 ',
            ),

            array(

                'name'=>'Mahfuz',

                'email'=>'mahfuzc@gmail.com',
                'created_at' => '2016-10-30 ',
                'updated_at' => '2016-10-30 ',
            ),

            array(

                'name'=>'Mahfuz',

                'email'=>'themahf1@gmail.com',
                'created_at' => '2016-10-30 ',
                'updated_at' => '2016-10-30 ',
            )

        );

        $faker=Faker::create();
        for($i=0;$i<5;$i++) {
            $data_array3 = array(

                array(

                    'name' => $faker->name,

                    'email' => $faker->email,
                    'created_at' => $faker->dateTime,
                    'updated_at' => $faker->dateTime,
                )


            );
           DB::table('users')->insert($data_array3);
        }

    //model factory usage
        //factory(App\User::class,2)->create();
    }

}
