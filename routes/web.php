<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});
Route::get('/marks', function () {
    return view('result.marks');

});
Route::post('/grade', function () {
    return view('result.grade');

});
Route::get('/about', function () {

    return view('blog.about');

});
Route::get('/home', function () {
    return view('blog.Home');

});
Route::get('/contact', function () {
    return view('blog.contact');

});
Route::get('/s', function () {
    $name = "Rabbi";
    $team =['Alpha','Beta','Gamma','Theta'];
    return view('blog.s', ['n'=> $name, 't'=> $team ]);
    //return view('blog.s');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
